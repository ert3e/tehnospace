<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<?php if ($type == 'logout') : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
<?php if ($params->get('greeting')) : ?>
	<div class="login-greeting">
	<?php if($params->get('name') == 0) : {
		echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name')));
	} else : {
		echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('username')));
	} endif; ?>
	</div>
<?php endif; ?>
<?php
$login = $user->get('username');
$db =& JFactory::getDBO();
$db->setQuery("SELECT * FROM bonus WHERE login = '$login'");
$datavalue = $db->loadAssocList();
//print_r($datavalue);
if(!empty($datavalue)) : ?>
	<span>на вашем счеу <?php echo $datavalue[0]['bonus']; ?> бонусных грн</span>
<? endif; ?>
	<div class="logout-button">
		<input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGOUT'); ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<?php else : ?>

<button class="btn btn-primary btn-block" data-toggle="modal" data-target="#login-form-modal">Войти в кабинет</button>

<div class="modal fade" id="login-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Вход в личный кабинет</h4>
      </div>
      <div class="modal-body">

		<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
			<?php if ($params->get('pretext')): ?>
			<div class="pretext">
				<p><?php echo $params->get('pretext'); ?></p>
			</div>
			<?php endif; ?>
			<fieldset class="userdata">
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input id="modlgn-username" type="text" name="username" class="form-control" />
				</div>
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input id="modlgn-passwd" type="password" name="password" class="form-control" />
				</div>
					
					<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
				 <div class="checkbox">
					<label for="modlgn-remember">
						<input id="modlgn-remember" type="checkbox" name="remember" value="yes"/>
						<?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?>
					</label>
				</div>
					
				<?php endif; ?>
				<input type="submit" name="Submit" class="btn btn-success" value="<?php echo JText::_('JLOGIN') ?>" />
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.login" />
				<input type="hidden" name="return" value="<?php echo $return; ?>" />
				<?php echo JHtml::_('form.token'); ?>
					
			</fieldset>
			
			<div class="pass-reg">
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
						<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
				
				<?php /* <a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
						<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?></a> */ ?>
				
				<?php $usersConfig = JComponentHelper::getParams('com_users');
				if ($usersConfig->get('allowUserRegistration')) : ?>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
					<?php echo JText::_('MOD_LOGIN_REGISTER'); ?></a>
				<?php endif; ?>
			</div>
			
			<?php if ($params->get('posttext')): ?>
				<div class="posttext">
					<p><?php echo $params->get('posttext'); ?></p>
				</div>
			<?php endif; ?>
			
		</form>
		</div>
    </div>
  </div>
</div>
<?php endif; ?>
