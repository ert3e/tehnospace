<?php 
defined('_JEXEC') or die('Restricted access');

$cache = JFactory::getCache('com_virtuemart','callback');
$vendorId = !isset($vendorId) || empty($vendorId) ? '1' : abs((int)$vendorId);
$document = JFactory::getDocument();
$document->addScriptDeclaration($js);
$document->addStyleDeclaration("#VMmenu".$ID." li.vmOpen>ul.menu".$class_sfx."{display: block;} #VMmenu".$ID." li.VmClose>ul.menu".$class_sfx."{display: none;}");

if(!function_exists('vm_template_get_tree_recurse')){
	function vm_template_get_tree_recurse($category,$childs,$parentCategories,$vendorId,$class_sfx,$ID,$level = 0){	
		$cache = JFactory::getCache('com_virtuemart','callback');
		$content = '';
		
		if(is_array($childs) && sizeof($childs)):
			++$level;
			ob_start(); ?>
			
			<ul class="sub-menu<?php echo $class_sfx; ?> vm-catelgries-level-<?php echo $level; ?>">
				<?php foreach ($childs as $child) :	?>
					<?php $active_menu = ''; ?>
					<?php $caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$child->virtuemart_category_id); ?>
					<?php $cattext = $child->category_name; ?>
					<?php if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = "active"; ?>
					<?php $child->childs = $cache->call( array( 'VirtueMartModelCategory', 'getChildCategoryList' ),$vendorId, $child->virtuemart_category_id ); ?>
				
					<li class="ul-block vm-category-<?php echo $child->virtuemart_category_id; ?>">
						<?php echo JHTML::link($caturl, $cattext); ?>
						<?php if (is_array($child->childs) && sizeof($child->childs)) : ?>					
							<?php echo vm_template_get_tree_recurse($child,$child->childs,$parentCategories,$vendorId,$class_sfx,$ID,$level); ?>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
			
<?php 
		$content = ob_get_contents();
		ob_end_clean();
		endif;
		
		return $content;
	}
} 

?>

<ul class="main-menu nav nav-tabs nav-justified<?php echo $class_sfx ?>">
<?php foreach ($categories as $category) { ?>
	<?php $active_menu = ''; ?>
	<?php $caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id); ?>
	<?php $cattext = $category->category_name; ?>
	<?php if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="active"'; ?>

	<li <?php echo $active_menu ?>>

		<?php echo JHTML::link($caturl, $cattext); ?>
		<?php if(is_array($category->childs) && sizeof($category->childs)){ ?>
			<?php echo vm_template_get_tree_recurse($category,$category->childs,$parentCategories,$vendorId,$class_sfx,$ID); ?>
		<?php } ?>
	</li>
<?php } ?>
</ul>
