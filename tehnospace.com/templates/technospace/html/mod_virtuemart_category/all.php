<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );
?>

<ul class="main-menu nav nav-tabs nav-justified<?php echo $class_sfx ?>" >
	<?php foreach ($categories as $category) {
		$active_menu = '';
		$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
		$cattext = $category->category_name; 
		if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="active"'; ?>

		<li <?php echo $active_menu ?>>
			<?php echo JHTML::link($caturl, $cattext); ?>
			<?php if ($category->childs ) { ?>

			<?php } ?>
		</li>
	<?php } ?>
</ul>
