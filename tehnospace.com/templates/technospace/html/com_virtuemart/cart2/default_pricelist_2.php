<?php 
defined('_JEXEC') or die('Restricted access');

JHTML::stylesheet ( 'plugins/system/onepage/onepage.css');
// Check to ensure this file is included in Joomla!
$plugin=JPluginHelper::getPlugin('system','onepage');
$params=new JRegistry($plugin->params);
$this->params=$params;

if($this->params->get('address_position',0)==0) {
	echo $this->loadTemplate('address'); 
}

$i=1;
?>
<div class="crt_top_bot"></div>
<table>
<?php 
$i=1;
foreach( $this->cart->products as $pkey =>$prow ) { ?>

				


<?php 
$i = 1 ? 2 : 1;
} ?>

<?php
$i=1;
foreach($this->cart->products as $pkey => $prow) { ?>

<div style="display:none;">
<?php
echo "<span class='priceColor2' id='subtotal_tax_amount_".$pkey."'>".$this->currencyDisplay->createPriceDiv('taxAmount','', $this->cart->pricesUnformatted[$pkey],false,false,$prow->quantity)."</span>";
echo "<span class='priceColor2' id='subtotal_discount_".$pkey."'>".$this->currencyDisplay->createPriceDiv('discountAmount','', $this->cart->pricesUnformatted[$pkey],false,false,$prow->quantity)."</span>";
?>
</div>

<div class="crt_all_block">
	<div class="crt_close"><a href="javascript:void(0)" onclick="update_form('remove_product','<?php echo $pkey; ?>')"></a></div>
	<div class="crt_block">
		<div class="crt_img"><?php if(!empty($prow->image)) echo $prow->image->displayMediaThumb('',false); ?></div>
		<div class="crt_info">
			<div class="crt_title"><?php echo JHTML::link($prow->url, $prow->product_name).$prow->customfields; ?></div>
			<div class="crt_bottom">
				<div class="crt_price"><?php echo $prow->product_price / 1; ?> грн</div>
				<div class="crt_count">
				<input type="text" title="<?php echo  JText::_('COM_VIRTUEMART_CART_UPDATE') ?>" class="inputbox" size="3" maxlength="4" value="<?php echo $prow->quantity ?>" id='quantity_<?php echo $pkey; ?>'/>
				<input type="button" class="vmicon vm2-add_quantity_cart" name="update" title="<?php echo  JText::_('COM_VIRTUEMART_CART_UPDATE') ?>" align="middle" onclick="update_form('update_product','<?php echo $pkey; ?>');"/>
				</div>
				<div class="crt_price_total" id="subtotal_with_tax_<?php echo $pkey; ?>"><?php echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity); ?></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<? $i = 1 ? 2 : 1;
} ?>
</table>
<div class="crt_top_bot"></div>

<?php if (VmConfig::get('coupons_enable')) { ?>
<div class="w50 my_cart_cupon">			
<?php echo JText::_('COM_VIRTUEMART_COUPON_CODE_ENTER') ?>
<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
	echo $this->loadTemplate('coupon');
}?>

<?php
	echo "<span id='coupon_code_txt'>".@$this->cart->cartData['couponCode']."</span>";
	echo @$this->cart->cartData['couponDescr'] ? (' (' . $this->cart->cartData['couponDescr'] . ')' ): '';
?>
</div>
<?php } ?>

<div class="w50 my_cart_all_total">
<p><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></p>
<?php echo $this->currencyDisplay->createPriceDiv('salesPrice','', $this->cart->pricesUnformatted,false) ?>
</div>
<div class="clear"></div>

<div class="crt_top_bot"></div>

<div class="w50 dostavka">
	<p><?php echo JText::_('COM_VIRTUEMART_CART_SELECTSHIPMENT');?></p>
	<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
		echo "<fieldset id='shipments' class='radio-check'>";					
		foreach($this->helper->shipments_shipment_rates as $rates) {
			echo str_replace("input",'input onclick="update_form();"',$rates)."<br />";
		}
		echo "</fieldset>";
	}
	else {
		JText::_('COM_VIRTUEMART_CART_SHIPPING');
	} ?>
</div>

<div class="w50 oplata">
	<p><?php echo JText::_('COM_VIRTUEMART_CART_SELECTPAYMENT');?></p>
	<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
		echo "<fieldset id='payments' class='radio-check'>";
		foreach($this->helper->paymentplugins_payments as $payments) {
			echo str_replace('type="radio"','type="radio" onclick="update_form();"',$payments)."<br />";
		}
		echo "</fieldset>";
	}
	else {
		JText::_('COM_VIRTUEMART_CART_PAYMENT'); 
	}
	?>
</div>
<div class="clear"></div>

<div class="crt_top_bot"></div>

<?php 
if($this->params->get('address_position',0)==1) {
	echo $this->loadTemplate('address'); 
}
?>
