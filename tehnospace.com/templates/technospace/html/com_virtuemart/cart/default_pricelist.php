<?php defined ('_JEXEC') or die('Restricted access');

?>
<div class="billto-shipto" style="display:none;">
	<div class="width50 floatleft">

		<span><span class="vmicon vm2-billto-icon"></span>
			<?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?></span>
		<?php // Output Bill To Address ?>
		<div class="output-billto" id="output-billto">
			<?php

			foreach ($this->cart->BTaddress['fields'] as $item) {
				if (!empty($item['value'])) {
					if ($item['name'] === 'agreed') {
						$item['value'] = ($item['value'] === 0) ? JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_NO') : JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_YES');
					}
					?><!-- span class="titles"><?php echo $item['title'] ?></span -->
					<span class="values vm2<?php echo '-' . $item['name'] ?>"><?php echo $this->escape ($item['value']) ?></span>
					<?php if ($item['name'] != 'title' and $item['name'] != 'first_name' and $item['name'] != 'middle_name' and $item['name'] != 'zip') { ?>
						<br class="clear"/>
						<?php
					}
				}
			} ?>
			<div class="clear"></div>
		</div>

		<a class="details output-billto-edit" id="output-billto-edit" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT', $this->useXHTML, $this->useSSL) ?>" rel="nofollow">
			<?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?>
		</a>

		<input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
	</div>

	<div class="width50 floatleft">

		<span><span class="vmicon vm2-shipto-icon"></span>
			<?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_SHIPTO_LBL'); ?></span>
		<?php // Output Bill To Address ?>
		<div class="output-shipto" id="output-shipto">
			<?php
			if (empty($this->cart->STaddress['fields'])) {
				echo JText::sprintf ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_EXPLAIN', JText::_ ('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'));
			} else {
				if (!class_exists ('VmHtml')) {
					require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php');
				}
				echo JText::_ ('COM_VIRTUEMART_USER_FORM_ST_SAME_AS_BT');
				echo VmHtml::checkbox ('STsameAsBTjs', $this->cart->STsameAsBT) . '<br />';
				?>
				<div id="output-shipto-display">
					<?php
					foreach ($this->cart->STaddress['fields'] as $item) {
						if (!empty($item['value'])) {
							?>
							<!-- <span class="titles"><?php echo $item['title'] ?></span> -->
							<?php
							if ($item['name'] == 'first_name' || $item['name'] == 'middle_name' || $item['name'] == 'zip') {
								?>
								<span class="values<?php echo '-' . $item['name'] ?>"><?php echo $this->escape ($item['value']) ?></span>
								<?php } else { ?>
								<span class="values"><?php echo $this->escape ($item['value']) ?></span>
								<br class="clear"/>
								<?php
							}
						}
					}
					?>
				</div>
				<?php
			}
			?>
			<div class="clear"></div>
		</div>
		<?php if (!isset($this->cart->lists['current_id'])) {
		$this->cart->lists['current_id'] = 0;
	} ?>
		<a class="details output-shipto-add" id="output-shipto-add" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'], $this->useXHTML, $this->useSSL) ?>" rel="nofollow">
			<?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?>
		</a>
	</div>

	<div class="clear"></div>
</div>


<?php
$i = 1;
// 		vmdebug('$this->cart->products',$this->cart->products);
foreach ($this->cart->products as $pkey => $prow) { ?>

<div class="crt_all_block">
	<div class="crt_close">
		<a class="vmicon vm2-remove_from_cart" title="<?php echo JText::_ ('COM_VIRTUEMART_CART_DELETE') ?>" align="middle" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=delete&cart_virtuemart_product_id=' . $prow->cart_item_id) ?>" rel="nofollow"></a>
	</div>
	<div class="crt_block">
		<div class="crt_img"><?php if(!empty($prow->image)) echo $prow->image->displayMediaThumb('',false); ?></div>
		<div class="crt_info">
			<div class="crt_title"><?php echo JHTML::link($prow->url, $prow->product_name).$prow->customfields; ?></div>
			<div class="crt_bottom">
				<div class="crt_price"><?php echo $prow->product_price / 1; ?> грн</div>
				<div class="crt_count">
<?php 
if ($prow->step_order_level)
	$step=$prow->step_order_level;
else
	$step=1;
if($step==0)
	$step=1;
$alert=JText::sprintf ('COM_VIRTUEMART_WRONG_AMOUNT_ADDED', $step);
?>
<script type="text/javascript">
function check<?php echo $step?>(obj) {
// use the modulus operator '%' to see if there is a remainder
remainder=obj.value % <?php echo $step?>;
quantity=obj.value;
if (remainder  != 0) {
	alert('<?php echo $alert?>!');
	obj.value = quantity-remainder;
	return false;
}
return true;
}
</script>

					<input type="text" onblur="check<?php echo $step?>(this);" onclick="check<?php echo $step?>(this);" onchange="check<?php echo $step?>(this);"  onsubmit="check<?php echo $step?>(this);" title="<?php echo  JText::_('COM_VIRTUEMART_CART_UPDATE') ?>" class="crt_count_input" size="3" maxlength="4" name="quantity[<?php echo $prow->cart_item_id ?>]" value="<?php echo $prow->quantity ?>" />
					<input type="submit" class="vmicon vm2-add_quantity_cart" name="update[<?php echo $prow->cart_item_id ?>]" title="<?php echo  JText::_ ('COM_VIRTUEMART_CART_UPDATE') ?>" align="middle" value=""/>
				</div>
				<div class="crt_price_total" id="subtotal_with_tax_<?php echo $pkey; ?>"><?php echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity); ?></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php
	$i = ($i==1) ? 2 : 1;
} ?>
<div class="crt_top_bot"></div>


<div class="w50 my_cart_all_total">
	<p><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></p>
	<?php echo $this->currencyDisplay->createPriceDiv('salesPrice','', $this->cart->pricesUnformatted,false) ?>
</div>
<?php // Continue and Checkout Button ?>
		<div class="checkout-button-top">

			<?php // Terms Of Service Checkbox
			if (!class_exists ('VirtueMartModelUserfields')) {
				require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'userfields.php');
			}
			$userFieldsModel = VmModel::getModel ('userfields');
			if ($userFieldsModel->getIfRequired ('agreed')) {
					if (!class_exists ('VmHtml')) {
						require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php');
					}
					echo VmHtml::checkbox ('tosAccepted', $this->cart->tosAccepted, 1, 0, 'class="terms-of-service"');

					if (VmConfig::get ('oncheckout_show_legal_info', 1)) {
						?>
						<div class="terms-of-service">

							<label for="tosAccepted">
								<a href="<?php JRoute::_ ('index.php?option=com_virtuemart&view=vendor&layout=tos&virtuemart_vendor_id=1', FALSE) ?>" class="terms-of-service" id="terms-of-service" rel="facebox"
							  	 target="_blank">
									<span class="vmicon vm2-termsofservice-icon"></span>
									<?php echo JText::_ ('COM_VIRTUEMART_CART_TOS_READ_AND_ACCEPTED'); ?>
								</a>
							</label>

							<div id="full-tos">
								<h2><?php echo JText::_ ('COM_VIRTUEMART_CART_TOS'); ?></h2>
								<?php echo $this->cart->vendor->vendor_terms_of_service; ?>
							</div>

						</div>
						<?php
					}
			}
			echo $this->checkout_link_html;
			?>
		</div>

<div class="clear"></div>

<div class="crt_top_bot" style="display:none;"></div>



<div class="w50 dostavka" style="display:none;">
	<p><?php echo JText::_('COM_VIRTUEMART_CART_SELECTSHIPMENT');?></p>
	<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
		echo "<fieldset id='shipments' class='radio-check'>";					
		foreach($this->helper->shipments_shipment_rates as $rates) {
			echo str_replace("input",'input onclick="update_form();"',$rates)."<br />";
		}
		echo "</fieldset>";
	}
	else {
		JText::_('COM_VIRTUEMART_CART_SHIPPING');
	} ?>
</div>

<div class="w50 oplata" style="display:none;">
	<p><?php echo JText::_('COM_VIRTUEMART_CART_SELECTPAYMENT');?></p>
	<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
		echo "<fieldset id='payments' class='radio-check'>";
		foreach($this->helper->paymentplugins_payments as $payments) {
			echo str_replace('type="radio"','type="radio" onclick="update_form();"',$payments)."<br />";
		}
		echo "</fieldset>";
	}
	else {
		JText::_('COM_VIRTUEMART_CART_PAYMENT'); 
	}
	?>
</div>
<div class="clear"></div>

<div class="crt_top_bot"></div>

<br>

<?php if (VmConfig::get('coupons_enable')) { ?>
<div class="w50 my_cart_cupon">
<a href="#" class="coupon_cart_none" onclick="cartshow('coupon_none', 30, 5)"><?php echo JText::_('COM_VIRTUEMART_COUPON_CODE_ENTER') ?></a>
<div id="coupon_none" style="display:none">
<?php if(!empty($this->layoutName) && $this->layoutName=='default') {
	echo $this->loadTemplate('coupon');
}?>

<?php
	echo "<span id='coupon_code_txt'>".@$this->cart->cartData['couponCode']."</span>";
	echo @$this->cart->cartData['couponDescr'] ? (' (' . $this->cart->cartData['couponDescr'] . ')' ): '';
?>
</div>
</div>
<?php } ?>

<body>
<!-- Google Code for &#1050;&#1086;&#1085;&#1074;&#1077;&#1088;&#1089;&#1080;&#1103; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 945221797;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "lEEoCKLTo2AQpeHbwgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/945221797/?label=lEEoCKLTo2AQpeHbwgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
/*<![CDATA[*/ var s=[],s_timer=[]; function cartshow(id,h,spd) { s[id]= s[id]==spd? -spd : spd; s_timer[id]=setTimeout(function() { var obj=document.getElementById(id);
if(obj.offsetHeight+s[id]>=h){obj.style.height=h+"px";
obj.style.overflow="auto";} else if(obj.offsetHeight+s[id]<=0){obj.style.height=0+"px";
obj.style.display="none";} else {obj.style.height=(obj.offsetHeight+s[id])+"px";
obj.style.overflow="hidden"; obj.style.display="block";
setTimeout(arguments.callee, 10); } }, 10); } /*]]>*/ </script>
</body> 
