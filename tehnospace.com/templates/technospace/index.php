﻿<?php 
defined( '_JEXEC' ) or die;

if ($this->countModules('left-block') != 0 && $this->countModules('right-block') != 0)
	$content = 'col-lg-6';
elseif ($this->countModules('left-block') == 0 && $this->countModules('right-block') == 0)
	$content = 'col-lg-12';



JHTML::script('bootstrap.js', 'templates/technospace/js/', false);


JHTML::stylesheet('magnific-popup.css', 'templates/technospace/css/', false);
JHTML::stylesheet('bootstrap.min.css', 'templates/technospace/css/', false);
JHTML::stylesheet('bootstrap-theme.min.css', 'templates/technospace/css/', false);
JHTML::stylesheet('font-awesome.css', 'templates/technospace/css/', false);
JHTML::stylesheet('non-responsive.css', 'templates/technospace/css/', false);
JHTML::stylesheet('adaptive.css', 'templates/technospace/css/', false);

?>

<!DOCTYPE html>
<html>
<head>
	<jdoc:include type="head" />
	<link rel="stylesheet" href="/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
<meta name="google-site-verification" content="nmpyb63kSAJCWAOBpBVh0ZFp8KQIU7aG_0TgEoFP78Y" />
<meta name='yandex-verification' content='42c8bae82d773b73' />
<meta name="msvalidate.01" content="B926D5954862F933B97FD6EDA5349BB9" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body data-spy="scroll" data-target="#up" id="wrap">
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-fixed-top mobile-top-menu " id="mobile-top-menu">
			<a href="#top-menu"class="burger-btn">
				<img src="http://tehnospace.com/templates/technospace/images/burger.png" alt="" class="m-nav-icon">
            </a>
            <a href="#off-menu_156" class="button-top-menu">
            	<button type="button" id="button-top-menu" class=" btn-success btn dropdown-toggle ">Каталог</button>
            </a>
            <ul  class="user-tools">
            	<li class="user-tools-i">
            		<a href="http://tehnospace.com/cart" class="user-tools-link">
            			<img src="http://tehnospace.com/templates/technospace/images/ShopingCart.png" alt="" class="search-icon">
            			<!--<span class="tems-count items-count"></span>-->
            		</a>
            	</li>
            	<li class="user-tools-i">
            		<a href="#search" class="user-tools-link  search-button" id="search-hide">
            			<img src="http://tehnospace.com/templates/technospace/images/search.png" alt="" class="search-icon">
            		</a>
            	</li>
            </ul>
        </nav>
 	<div class="hidden-mobile"> 

 		<div id="top-menu-not">    
			<jdoc:include type="modules" name="top-menu" style="none" />
		</div>
	</div>
        <div class="hidden">
        	<div id="top-menu" class="top-menu black-popup">
        		<div class="popup">
        			<div class="site-name-popup">
        				<span class="site-name"><?php echo JFactory::getApplication()->getCfg('sitename'); ?></span>
						<jdoc:include type="modules" name="logo" />
        			</div>	
					<ul>
						<li class="login-icon">
							<a data-toggle="modal" data-target="#login-form-modal"> Войти в кабинет</a>
						</li>
						<li class="korzina-icon">
							<a href="http://tehnospace.com/cart">Корзина</a>
						</li>
					</ul>
        			<jdoc:include type="modules" name="top-menu" style="none" />
        		</div>
        	</div>
        </div>

		<header >
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-3 hidden-none-mobile">
						<div id="logo" >	
							<a href="/">
								<span class="site-name"><?php echo JFactory::getApplication()->getCfg('sitename'); ?></span>
								<jdoc:include type="modules" name="logo" />
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 hidden-none-mobile">
						<div class="search-position-nontop">
							<div id="search">
								<div class="search-position-top">
									<jdoc:include type="modules" name="search" />
								</div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-3 hidden-mobile">
						<div id="logo" >	
							<a href="/">
								<span class="site-name"><?php echo JFactory::getApplication()->getCfg('sitename'); ?></span>
								<jdoc:include type="modules" name="logo" />
							</a>
						</div>
					</div>
			
					<div class="col-xs-12 col-sm-6 hidden-mobile">
						<div id="search">
							<jdoc:include type="modules" name="search" />
						</div>
					</div>

					<div class="col-xs-12 col-sm-3 ">
						<div id="korzina">
							<jdoc:include type="modules" name="korzina" />
						</div>
					</div>
					
				</div>
			</div>
			
		</header>
		<?php if (JURI::current() == JURI::base()) : ?>
			<h1 style="display:none;">Tehnospace.com.ua - ноутбук, смартфон, фотоаппарат, телевизор, видеокамера, видеорегистратор, техника для кухни, блендер, вытяжка, духовой шкаф и многое другое в нашем каталоге</h1>
		<?php endif; ?>
		<?php
		//<div class="container-fluid">
			//<div class="row">

				//<div class="col-lg-12">
					  	//<div id="menu">
						// <jdoc:include type="modules" name="menu" />
						//</div>
				//</div>
			//</div>
		//</div>?>
		<?php if (JURI::current() != JURI::base()) : ?>
		<div class="poloska">
			<?php if (JURI::current() != JURI::base()) : ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php if($this->countModules('navigator') != 0) : ?>
						<div id="navigator">
							<jdoc:include type="modules" name="navigator" />
						</div>
						<?php endif; ?>
						
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<div class="container-fluid" id="main">
			<div class="row">				
				<div class="col-lg-10 col-xs-10">

						<?php if($this->countModules('left-block') != 0) : ?>
						<div id="left-block">			
						<button class="vm-button-left-menu hidden-mobile"  onmouseover="show('menu-left',290,5)">Каталог товаров </button>

							<?php if (JURI::current() == JURI::base()) : ?>
								<div id="menu" style="display:block">
									<jdoc:include type="modules" name="menu-left" style="..." />

 								</div>
								<?php endif; ?>
							<?php if (JURI::current() != JURI::base()) : ?>
								<div id="menu-left" style="display:none">
									<jdoc:include type="modules" name="menu-left" style="xhtml" />
 								</div>
								<?php endif; ?>

								<?php if (JURI::current() != JURI::base()) : ?>	
								<jdoc:include type="modules" name="filter" style="xhtml" />
								<?php endif; ?>
							<jdoc:include type="modules" name="left-block" style="xhtml" />
						</div>
					<?php endif; ?>
										
					<div id="content">
						<?php if (JURI::current() == JURI::base()) : ?>
						<?php if($this->countModules('slider') != 0) : ?>
						<div id="slider">
							<jdoc:include type="modules" name="slider" />
						</div>
						<?php endif; ?>
					<?php else : ?>
						<div class="br"></div>
					<?php endif; ?>

						<jdoc:include type="message" />
						<jdoc:include type="component" />
						
						<jdoc:include type="modules" name="content-bottom" style="xhtml" />
					</div>
					
				</div>
				
				<?php if($this->countModules('right-block') != 0) : ?>
					<div class="col-lg-2 col-xs-2">
						<div class="br"></div>
						<div id="right-block">
                            <div class="moduletable">
                                <div class="custom">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td><span style="font-size: small;"><img src="images/delivery.jpg" alt="Доставка" title="Доставка"/></span></td>
                                            <td class="system-pagebreak"><span style="font-family: 'arial black', 'avant garde';"><strong><span style="font-size: small; color: #000000;">БЫСТРАЯ ДОСТАВКА</span></strong></span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p style="color: #000000;"><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: small; line-height: 1.3em;">В любой уголок </span><strong class="black" style="font-family: tahoma, arial, helvetica, sans-serif; font-size: small; line-height: 1.3em;">Украины</strong><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: small; line-height: 1.3em;">:</span></p>
                                    <p>
                                        <span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">— <strong style="color: #000000;">Самовывоз Новая Почта</strong> - (по тарифам Новой Почты, от 30 грн)</span><br /><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">— Курьер Новая Почта - (по тарифам Новой Почты)</span><br /><span style="font-size: x-small; font-family: tahoma, arial, helvetica, sans-serif;">— Другой перевозчик по договоренности с оператором<br /><span>— <span style="color: #fe000a;"> New </span>Доставка аксессуаров Уркпочтой от <strong>12 грн</strong> </span></span></p>
                                    <p>
                                        <span style="font-size: x-small; font-family: tahoma, arial, helvetica, sans-serif;"> </span>
                                        <strong>
                                            <span style="font-size: small; font-family: tahoma, arial, helvetica, sans-serif;">Сумы:
                                            </span>
                                        </strong> 
                                    </p>
                                    <p>
                                        <span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">— Самовывоз - <span class="up bold black"><strong>БЕСПЛАТНО!</strong>*</span></span><br /><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">— Курьер - при заказе от 1000 грн.  <strong><span class="bold black up">БЕСПЛАТНО!</span> </strong><span class="star_info">*</span></span><br /><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">— Курьер - при заказе до 1000 грн. - 100 грн.</span><br /><span style="line-height: 1.3em; font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;">* Доставка крупногабаритных грузов - 100 грн. </span><span style="line-height: 1.3em; font-family: tahoma, arial, helvetica, sans-serif; font-size: x-small;"> </span></p>
                                    <table>
							<tbody>
                            <tr>
                                <td rowspan="2">
                                    <span style="font-size: small; font-family: 'arial black', 'avant garde';">
                                        <img src="images/guarantee.jpg" alt="Гарантия" title="Гарантия" style="font-size: 12.16px; line-height: 1.3em;" /> 
                                    </span>
                                </td>
                                <td class="caption">
                                    <p>
                                        <span style="font-family: 'arial black', 'avant garde'; font-size: x-small;">
                                            <strong style="color: #000000;">ОФИЦИАЛЬНАЯ ГАРАНТИЯ</strong>
                                        </span>
                                    </p>
                                </td>
							</tr>
                            <tr>
                                <td class="caption">
                                    <p><span style="font-family: 'arial black', 'avant garde'; font-size: x-small;"><strong style="color: #000000;">ОТ ПРОИЗВОДИТЕЛЯ </strong></span></p>
                                </td>
							</tr>
                            </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td rowspan="2">
                                                <img src="images/14.jpg" alt="Возврат-обмен" title="Возврат-обмен"/>
                                            </td>
                                            <td>
                                                <div class="title">
                                                    <span style="font-family: 'arial black', 'avant garde'; font-size: x-small;">
                                                        <strong style="color: #000000;">14 ДНЕЙ НА ОБМЕН</strong>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="descr">
                                                    <span style="font-size: x-small;">без лишних вопросов.</span>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
							</div>
						</div>
					</div>
				<?php endif; ?>
				
			</div>
		</div>
		
		<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<div id="left-footer-1">
						<jdoc:include type="modules" name="left-footer-1" style="xhtml" />
					</div>
				</div>
				<div class="col-lg-3">
					<div id="left-footer-2">
						<jdoc:include type="modules" name="left-footer-2" style="xhtml" />
					</div>
				</div>
				<div class="col-lg-3">
					<div id="right-footer-1">
						<jdoc:include type="modules" name="right-footer-1" style="xhtml" />
					</div>
				</div>
				<div class="col-lg-3">
					<div id="right-footer-2">
						<div class="moduletable">
							<h3>Интернет-магазин в соц. сетях</h3>
							<div class="custom">
								<p><a href="http://vk.com/tehnospace" target="_blank" title="ВКонтакте"><img src="images/VK.png" alt="ВКонтакте" title="ВКонтакте"/></a><a href="https://www.facebook.com/Tehnospace" target="_blank" title="Facebook"><img src="images/FS.png" alt="Facebook" title="Facebook"/></a><span style="font-size: 12.16px; line-height: 1.3em;">Присоединяйтесь к </span><strong style="font-size: 12.16px;line-height: 1.3em;">интернет-магазину</strong><span style="font-size: 12.16px; line-height: 1.3em;"> в социальных сетях. Вы будете в курсе технологических новинок, будете знать о наших лидерах продаж, будете первыми кто узнает о всех наших акциях и распродажах.</span></p>
									<p>Акции в Tehnospace.com.ua не заканчиваются некогда.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</footer>
		
	</div>
	<div id="up">
	<a href="#top"><i class="fa fa-angle-up"></i><br>Наверх</a>
</div>

    <script src="/templates/technospace/js/jquery-1.11.1.min.js"></script>

    <script src="/templates/technospace/js/jquery.magnific-popup.min.js"></script>
    <script src="/templates/technospace/js/common.js"></script>
<script>
/*<![CDATA[*/ var s=[],s_timer=[]; function show(id,h,spd) { s[id]= s[id]==spd? -spd : spd; s_timer[id]=setTimeout(function() { var obj=document.getElementById(id);
if(obj.offsetHeight+s[id]>=h){obj.style.height=h+"px";
obj.style.overflow="auto";} else if(obj.offsetHeight+s[id]<=0){obj.style.height=0+"px";
obj.style.display="none";} else {obj.style.height=(obj.offsetHeight+s[id])+"px";
obj.style.overflow="hidden"; obj.style.display="block";
setTimeout(arguments.callee, 10); } }, 10); } /*]]>*/ </script>
<script type="text/javascript" charset="utf-8" src="/callme/js/callme.js"></script>
<!--Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64949602-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'o5dL9Fl4JE';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>

</html>